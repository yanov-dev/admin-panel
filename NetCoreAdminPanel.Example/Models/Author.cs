﻿using System;
using System.Collections.Generic;

namespace NetCoreAdminPanel.Example.Models
{
    public class Author
    {
        public string Id { get; set; }
        
        public DateTime Created { get; set; }
        
        public string Name { get; set; }
        
        public virtual ICollection<Book> Books { get; set; }
    }
}