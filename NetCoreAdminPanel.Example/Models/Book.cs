﻿namespace NetCoreAdminPanel.Example.Models
{
    public class Book
    {
        public string Id { get; set; }
        
        public Author Author { get; set; }
        
        public string AuthorId { get; set; }
        
        public string Name { get; set; }
        
        public int Price { get; set; }
    }
}