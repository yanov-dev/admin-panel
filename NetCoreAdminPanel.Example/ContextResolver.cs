﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdminPanel.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace NetCoreAdminPanel.Example
{
    public class ContextResolver : IContextResolver
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly Dictionary<string, Type> _contexts;

        public ContextResolver(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            _contexts = new Dictionary<string, Type>
            {
                ["example_context"] = typeof(ExampleDbContext)
                // add other contexts here
            };
        }

        public DbContext GetContext(string name)
        {
            if (_contexts.TryGetValue(name, out var context))
            {
                var score = _scopeFactory.CreateScope();
                return score.ServiceProvider.GetRequiredService(context) as DbContext;
            }

            throw new ArgumentException(nameof(name));
        }

        public IEnumerable<string> GetAllNames()
        {
            return _contexts.Keys.ToList();
        }
    }
}