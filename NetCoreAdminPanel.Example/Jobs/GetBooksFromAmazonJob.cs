﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AdminPanel.Jobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NetCoreAdminPanel.Example.Models;

namespace NetCoreAdminPanel.Example.Jobs
{
    public class GetBooksFromAmazonJob : BaseJob
    {
        protected override async Task DoWork(IServiceScopeFactory scopeFactory, CancellationToken ct)
        {
            using (var scope = scopeFactory.CreateScope())
            {
                var exampleContext = scope.ServiceProvider.GetRequiredService<ExampleDbContext>();

                var rand = new Random();

                var authorsCount = exampleContext.Authors.Count();

                var randomAuthor = await exampleContext.Authors.Skip(rand.Next(authorsCount)).FirstAsync();

                var newBook = CreateBookFromAmazon();

                newBook.AuthorId = randomAuthor.Id;

                exampleContext.Books.Add(newBook);
                await exampleContext.SaveChangesAsync();
            }
        }

        private Book CreateBookFromAmazon()
        {
            var rand = new Random();

            return new Book
            {
                Id = Guid.NewGuid().ToString(),
                Name = $"Book #{rand.Next(10000)}",
                Price = rand.Next(1, 20)
            };
        }
    }
}