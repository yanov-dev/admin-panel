﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using NetCoreAdminPanel.Example.Models;

namespace NetCoreAdminPanel.Example
{
    public class ExampleDbContext : DbContext, IDesignTimeDbContextFactory<ExampleDbContext>
    {
        public ExampleDbContext(DbContextOptions<ExampleDbContext> options)
            : base(options)
        {
        }

        public ExampleDbContext()
        {
            
        }

        public DbSet<Book> Books { get; set; }

        public DbSet<Author> Authors { get; set; }

        public ExampleDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ExampleDbContext>();
            optionsBuilder.UseSqlite("Data Source=main.db");

            return new ExampleDbContext(optionsBuilder.Options);
        }
    }
}