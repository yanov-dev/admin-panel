﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AdminPanel;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NetCoreAdminPanel.Example.BackgroundServices;

namespace NetCoreAdminPanel.Example
{
    public class Startup
    {
        public Startup()
        {
            
        }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDirectoryBrowser();
            
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AdminPanelMapperProfile>();
            });
            
            services.AddTransient<IHostedService, InitService>();

            services.AddMvc();
            services.AddMvc().AddApplicationPart(Assembly.Load(new AssemblyName("AdminPanel")));
            
            services.AddDbContext<ExampleDbContext>(options =>
                options.UseSqlite("Data Source=main.sqlite"));
            services.AddAdminPanel<ContextResolver>(options =>
                options.UseSqlite($"Data Source=admin_panel.sqlite"));
            
            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseAdminPanel();
            app.UseStaticFiles();

            app.UseCors("MyPolicy");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}