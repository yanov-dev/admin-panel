﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace NetCoreAdminPanel.Example.Controllers
{
    [Route("app")]
    public class AppController : Controller
    {
        [HttpGet("{appName}/{*url}")]
        public async Task<IActionResult> Index(string appName)
        {
            var html = await System.IO.File.ReadAllTextAsync($"wwwroot\\app\\{appName}\\index.html")
                .ConfigureAwait(false);
            html = html
                .Replace("href=\"styles.", $"href=\"app/{appName}/styles.")
                .Replace("<script type=\"text/javascript\" src=\"",
                    $"<script type=\"text/javascript\" src=\"app/{appName}/");

            return View((object) html);
        }
    }
}