# Admin panel

This project show how you can include admin panel to your projects.

## What can it do?
* Run yours background jobs manually or via autorun
* Build dynamic ui into your browser. Available controls are (Blocks, Charts, Table, Tabs, Cards) All controls support caching
* Build your queries to display your data
 
You can watch demo into repository (Demo.mp4)

# This project is just pet project for the main site