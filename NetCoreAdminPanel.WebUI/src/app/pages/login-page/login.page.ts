import {Component} from '@angular/core';
import {AuthService} from '../../services/auth-service';
import {Router} from '@angular/router';

@Component({
  templateUrl: './login.page.html',
})
export class LoginPageComponent {
  userName: string = '';
  password: string = '';

  constructor(private _authService: AuthService, private _router: Router) {

  }

  loginClick() {
    this._authService.login(this.userName, this.password).subscribe(e => {
      const error = e.error;
      const jwt = e.jwt;

      if (jwt != null) {
        this._authService.setToken(jwt);
        this._router.navigate(['/']);
      } else {
      }
    });
  }
}
