import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from './pages/home-page/home.page';
import {AuthorizationGuard} from './services/auth-guard';
import {LoginPageComponent} from './pages/login-page/login.page';
import {SystemStatsComponent} from './components/system.stats.component/system.stats.component';
import {SettingsComponent} from './components/settings.component/settings.component';
import {JobsComponent} from './components/jobs.component/jobs.component';
import {SqlQueriesComponent} from './components/sql-queries/sql-queries.component';
import {LayoutEditorComponent} from './components/layout-editor/layout-editor.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'layout/:id',
    component: LayoutEditorComponent
  },
  {
    path: 'layout',
    component: LayoutEditorComponent
  },
  {
    path: 'home',
    component: HomePageComponent,
    children: [
      {
        path: '',
        component: SystemStatsComponent
      },
      {
        path: 'system',
        component: SystemStatsComponent
      },
      {
        path: 'settings',
        component: SettingsComponent,
        canActivate: [AuthorizationGuard],
      },
      {
        path: 'jobs',
        component: JobsComponent,
        canActivate: [AuthorizationGuard],
      },
      {
        path: 'sql-queries',
        component: SqlQueriesComponent
      }
    ]
  },
  {path: 'login', component: LoginPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

