import {BrowserModule} from '@angular/platform-browser';
import {NgModule, PipeTransform} from '@angular/core';

import {AppComponent} from './app.component';
import {
  MAT_DIALOG_DEFAULT_OPTIONS,
  MatButtonModule, MatCardModule, MatCheckboxModule, MatDialogModule, MatExpansionModule, MatIconModule, MatInputModule, MatListModule,
  MatPaginatorModule, MatProgressSpinnerModule, MatSelectModule,
  MatSidenavModule, MatSnackBarModule,
  MatTableModule, MatTabsModule,
  MatToolbarModule, MatTreeModule
} from '@angular/material';
import {APP_BASE_HREF} from '@angular/common';
import {HomePageComponent} from './pages/home-page/home.page';
import {LoginPageComponent} from './pages/login-page/login.page';
import {AppRoutingModule} from './app-routing.module';
import {AuthorizationGuard} from './services/auth-guard';
import {AuthService} from './services/auth-service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MediaMatcher} from '@angular/cdk/layout';
import {LogService} from './services/log.service';
import {AuthenticationInterceptor} from './services/auth-interceptor';
import {SystemStatsComponent} from './components/system.stats.component/system.stats.component';
import {SystemService} from './services/system.service';
import {KeysPipe} from './pipes/pipe.transform';
import {TagService} from './services/tag.service';
import {SettingsComponent} from './components/settings.component/settings.component';
import {SettingsService} from './services/settings.service';
import {JSONEditorModule} from 'ngx-jsoneditor';
import {ErrorsService} from './services/errors.service';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {JobsService} from './services/jobs.service';
import {JobsComponent} from './components/jobs.component/jobs.component';
import {DynamicTableComponent} from './components/dynamic/dynamic-table/dynamic-table.component';
import {ApiChartComponent} from './components/dynamic/dynamic-chart/dynamic-chart.component';
import {DynamicTabGroupComponent} from './components/dynamic/dynamic-tab-group/dynamic-tab-group-component';
import {DynamicComponent} from './components/dynamic/dynamic-component/dynamic-component';
import {DynamicCardComponent} from './components/dynamic/dynamic-card/dynamic-card.component';
import {DynamicDivComponent} from './components/dynamic/dynamic-div/dynamic-div.component';
import { EditDynamicDialogComponent } from './components/dynamic/editor/edit-dynamic-dialog/edit-dynamic-dialog.component';
import { EditDynamicButtonComponent } from './components/dynamic/editor/edit-dynamic-button/edit-dynamic-button.component';
import { AddDynamicDialogComponent } from './components/dynamic/editor/add-dynamic-dialog/add-dynamic-dialog.component';
import { DynamicLayoutComponent } from './components/dynamic/dynamic-layout/dynamic-layout.component';
import {SqlQueriesComponent} from './components/sql-queries/sql-queries.component';
import {AppSettingsService} from './services/app-settings-service';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';
import { LayoutEditorComponent } from './components/layout-editor/layout-editor.component';
import { MoveTreeNodeDialogComponent } from './components/dialogs/move-tree-node-dialog/move-tree-node-dialog.component';
import {EditQueryDialogComponent} from './components/dialogs/edit-query-dialog/edit-query-dialog.component';
import { NewNameDialogComponent } from './components/dialogs/new-name-dialog/new-name-dialog.component';
import { JobEditorDialogComponent } from './components/dialogs/job-editor-dialog/job-editor-dialog.component';
import { SqlExecuteDialogComponent } from './components/dialogs/sql-execute-dialog/sql-execute-dialog.component';

@NgModule({
  declarations: [
    DynamicComponent,
    KeysPipe,
    SystemStatsComponent,
    AppComponent,
    DynamicTabGroupComponent,
    SettingsComponent,
    HomePageComponent,
    JobsComponent,
    LoginPageComponent,
    ApiChartComponent,
    DynamicTableComponent,
    DynamicCardComponent,
    DynamicDivComponent,
    EditDynamicDialogComponent,
    EditDynamicButtonComponent,
    AddDynamicDialogComponent,
    DynamicLayoutComponent,
    SqlQueriesComponent,    
    EditQueryDialogComponent,
    LayoutEditorComponent,
    MoveTreeNodeDialogComponent,
    NewNameDialogComponent,
    JobEditorDialogComponent,
    SqlExecuteDialogComponent
  ],
  entryComponents: [
    SqlExecuteDialogComponent,
    JobEditorDialogComponent,
    NewNameDialogComponent,
    MoveTreeNodeDialogComponent,
    EditDynamicDialogComponent,
    AddDynamicDialogComponent,
    EditQueryDialogComponent
  ],
  imports: [
    LazyLoadImagesModule,
    MatTreeModule,
    MatSelectModule,
    MatDialogModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    NgxChartsModule,
    MatExpansionModule,
    MatDialogModule,
    MatPaginatorModule,
    MatTableModule,
    MatIconModule,
    MatListModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatSidenavModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    BrowserModule,
    AppRoutingModule,
    JSONEditorModule
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: '/app/admin'},
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true,
    },
    JobsService,
    ErrorsService,
    TagService,
    SystemService,
    LogService,
    AppSettingsService,
    MediaMatcher,
    AuthorizationGuard,
    AuthService,
    SettingsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
