export class QueryInfo {
  id: string;
  name: string;
  level: number;
  expandable: boolean;
  data: any;
  parentGroupId: string;
}
