import {SqlData} from './SqlData';

export class RawSqlResponse {
  queryCount: number;
  data: SqlData;
  error: string;
}


