import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';

@Injectable()
export class JobsService {
  constructor(private _http: HttpClient) {

  }

  update(job: any): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/jobs/update', job);
  }

  get(): Observable<any> {
    return this._http.get(environment.hostUrl + '/api/jobs');
  }

  start(name: string): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/jobs/start', {
      job_name: name
    });
  }

  stop(name: string): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/jobs/stop', {
      job_name: name
    });
  }
}
