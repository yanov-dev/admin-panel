import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';

@Injectable()
export class AuthService {
  TOKEN_KEY: string = 'token';

  constructor(private _http: HttpClient) {

  }

  logout() {
    localStorage.removeItem(this.TOKEN_KEY);
  }

  isLogined(): boolean {
    return this.getToken() != null;
  }

  setToken(jwt: string) {
    localStorage.setItem(this.TOKEN_KEY, jwt);
  }

  getToken(): string {
    return localStorage.getItem(this.TOKEN_KEY);
  }

  login(userName: string, password: string): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/account/token', {
      userName: userName,
      password: password
    });
  }
}
