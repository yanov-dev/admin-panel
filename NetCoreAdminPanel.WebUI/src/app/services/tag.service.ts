import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';

@Injectable()
export class TagService {
  constructor(private _http: HttpClient) {

  }

  recalculate(): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/tag/recalculate', {});
  }
}
