import {EventEmitter, Injectable, Output} from '@angular/core';

@Injectable()
export class AppSettingsService {
  _devMode: boolean;

  set devMode(value: boolean) {
    this._devMode = value;
    this.changed.emit(value);
  }

  get devMode() {
    return this._devMode;
  }

  @Output() changed: EventEmitter<boolean> = new EventEmitter();

  constructor() {

  }
}
