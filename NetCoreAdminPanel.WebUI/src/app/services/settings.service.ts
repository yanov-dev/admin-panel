import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';
import {Setting} from '../Models/setting';
import {map} from 'rxjs/operators';


@Injectable()
export class SettingsService {
  constructor(private _http: HttpClient) {

  }

  getAll(): Observable<Setting[]> {
    return this._http.get(environment.hostUrl + '/api/settings')
      .pipe(
        map(e => {
          return (<any[]>e).map(function (job: any) {
            var model = <Setting>job;
            model.dataObj = JSON.parse(model.data);
            return model;
          });
        })
      );
  }

  update(jobStat: Setting): Observable<any> {
    jobStat.data = JSON.stringify(jobStat.dataObj);
    return this._http.patch(environment.hostUrl + '/api/settings', jobStat);
  }
}
