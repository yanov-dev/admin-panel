import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';

@Injectable()
export class ErrorsService {
  constructor(private _http: HttpClient) {

  }

  get(offset: number, count: number): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/system/errors', {
      offset: offset,
      count: count
    });
  }
}
