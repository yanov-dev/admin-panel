import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';

import {Setting} from '../Models/setting';

import {map} from 'rxjs/operators';

@Injectable()
export class LogService {
  constructor(private _http: HttpClient) {

  }

  getActivityByDate(): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/log/date', {})
      .pipe(
        map(e => {
          return (<any[]>e).map(function (stat: any) {
            return {
              name: stat.item1,
              value: stat.item2
            };
          });
        })
      );
  }

  getActivityByTime(): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/log/time', {})
      .pipe(
        map(e => {
          return (<any[]>e).map(function (stat: any) {
            return {
              name: stat.item1,
              value: stat.item2
            };
          });
        })
      );
  }

  get(offset: number, count: number): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/log', {
      offset: offset,
      count: count
    });
  }
}
