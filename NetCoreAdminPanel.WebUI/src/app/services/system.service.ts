import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {environment} from '../../environments/environment';

@Injectable()
export class SystemService {
  constructor(private _http: HttpClient) {

  }

  getInfo(): Observable<any> {
    return this._http.get(environment.hostUrl + '/api/system');
  }

  shutdown(): Observable<any> {
    return this._http.post(environment.hostUrl + '/api/system/shutdown', {});
  }
}
