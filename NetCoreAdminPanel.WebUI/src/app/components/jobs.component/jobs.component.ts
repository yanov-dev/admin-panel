import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {JobsService} from '../../services/jobs.service';
import {JobEditorDialogComponent} from '../dialogs/job-editor-dialog/job-editor-dialog.component';
import {timer} from 'rxjs';

@Component({
  templateUrl: './jobs.component.html'
})
export class JobsComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<any>;
  displayedColumns = ['name', 'change_state', 'autorunEnabled', 'autorunMinutes', 'edit'];
  timer: any;

  constructor(private jobService: JobsService, public dialog: MatDialog) {

  }

  editJob(job: any) {
    const dialogRef = this.dialog.open(JobEditorDialogComponent, {
      data: job
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result)
        return;

      this.jobService.update(result).subscribe(e => {
        this.loadData();
      });
    });
  }

  ngOnDestroy() {
    this.timer.unsubscribe();
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<any>();
    this.timer = timer(1000, 1000).subscribe(e => this.loadData());
    this.loadData();
  }

  loadData() {
    this.jobService.get().subscribe(e => {
      this.dataSource.data = e;
    });
  }

  startJob(name: string) {
    this.jobService.start(name).subscribe(e => {
      this.loadData();
    });
  }

  stopJob(name: string) {
    this.jobService.stop(name).subscribe(e => {
      this.loadData();
    });
  }
}
