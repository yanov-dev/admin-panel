import {Component, OnInit} from '@angular/core';

import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {Observable, of as observableOf} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {RawSqlResponse} from '../../Models/RawSqlResponse';
import {environment} from '../../../environments/environment';
import {QueryInfo} from '../../Models/QueryInfo';
import {MatDialog} from '@angular/material';
import {AddDynamicDialogComponent} from '../dynamic/editor/add-dynamic-dialog/add-dynamic-dialog.component';
import {EditQueryDialogComponent} from '../dialogs/edit-query-dialog/edit-query-dialog.component';
import {MoveTreeNodeDialogComponent} from '../dialogs/move-tree-node-dialog/move-tree-node-dialog.component';
import {NewNameDialogComponent} from '../dialogs/new-name-dialog/new-name-dialog.component';

export class TreeFolder {
  parentGroupId: string;
  id: string;
  children: any[];
  name: string;
}

@Component({
  selector: 'app-sql-queries',
  templateUrl: './sql-queries.component.html'
})
export class SqlQueriesComponent {
  treeControl: FlatTreeControl<QueryInfo>;
  treeFlattener: MatTreeFlattener<TreeFolder, QueryInfo>;
  dataSource: MatTreeFlatDataSource<TreeFolder, QueryInfo>;

  editQuery(query: QueryInfo) {
    const dialogRef = this.dialog.open(EditQueryDialogComponent, {
      data: {
        query: query.data
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.reload();
    });
  }

  editGroup(node: any) {
    console.log(node);
    const dialogRef = this.dialog.open(NewNameDialogComponent, {
      data: node.name
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result)
        return;

      node.name = result;
      this.http.post(environment.hostUrl + '/api/sql/tree/group/update', node).subscribe(e => {
          this.reload();
        }
      );
    });
  }

  moveTreeEntity(node: any) {
    const dialogRef = this.dialog.open(MoveTreeNodeDialogComponent, {
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!result)
        return;

      this.http.post(environment.hostUrl + '/api/sql/tree/move', {
        id: node.id,
        parentId: result.id
      }).subscribe(e => {
          this.reload();
        }
      );
    });
  }

  createQuery() {
    this.http.post(environment.hostUrl + '/api/sql/query/create', {}).subscribe(e => {
        this.reload();
      }
    );
  }

  removeGroup(group: any) {
    this.http.post(environment.hostUrl + '/api/sql/tree/group/delete', group).subscribe(e => {
        this.reload();
      }
    );
  }

  createGroup() {
    this.http.post(environment.hostUrl + '/api/sql/tree/group/create', {}).subscribe(e => {
        this.reload();
      }
    );
  }

  constructor(private http: HttpClient, public dialog: MatDialog) {
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this._getLevel,
      this._isExpandable,
      this._getChildren);
    this.treeControl = new FlatTreeControl<QueryInfo>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    this.reload();
  }

  reload() {
    this.http.post(environment.hostUrl + '/api/sql/tree', {}).subscribe(e => {
        this.dataSource.data = <TreeFolder[]>e;
      }
    );
  }

  transformer = (node: TreeFolder, level: number) => {
    const query = new QueryInfo();
    query.name = node.name;
    query.id = node.id;
    query.level = level;
    query.expandable = !!node.children;
    query.data = node;
    query.parentGroupId = node.parentGroupId;
    return query;
  };

  private _getLevel = (node: QueryInfo) => {
    return node.level;
  };

  private _isExpandable = (node: QueryInfo) => {
    return node.expandable;
  };

  private _getChildren = (node: TreeFolder): Observable<TreeFolder[]> => {
    return observableOf(node.children);
  };

  hasChild = (_: number, _nodeData: QueryInfo) => {
    return _nodeData.expandable;
  };
}

