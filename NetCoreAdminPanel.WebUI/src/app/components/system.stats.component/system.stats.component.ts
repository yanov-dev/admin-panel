import {Component, OnInit} from '@angular/core';
import {SystemService} from '../../services/system.service';

@Component({
  templateUrl: './system.stats.component.html'
})
export class SystemStatsComponent {
  constructor(
    private _systemService: SystemService) {
  }

  shutdown() {
    this._systemService.shutdown().subscribe(e => {
      alert('system shutdowned');
    });
  }
}
