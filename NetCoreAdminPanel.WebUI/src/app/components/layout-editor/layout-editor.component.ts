///<reference path="../../../../node_modules/@angular/router/src/router.d.ts"/>
import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {AppSettingsService} from '../../services/app-settings-service';
import {AuthService} from '../../services/auth-service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-layout-editor',
  templateUrl: './layout-editor.component.html',
  styleUrls: ['./layout-editor.component.css']
})
export class LayoutEditorComponent implements OnInit {

  layoutId: string;
  layouts: any[];

  layout: any;

  _selectedLayout: any;

  get selectedLayout(): any {
    return this._selectedLayout;
  }

  @Input()
  set selectedLayout(layout: any) {
    this.router.navigate(['layout', layout.id]);
    this.layoutId = layout.id;
    this.selectLayout();
  }

  constructor(public appSettingsService: AppSettingsService,
              private route: ActivatedRoute,
              private http: HttpClient,
              private router: Router) {
  }

  update() {
    this.http.post(environment.hostUrl + '/api/admin/layout/', this.layout).subscribe(s => {
      this.loadLayouts();
    });
  }

  delete() {
    this.http.post(environment.hostUrl + '/api/admin/layout/delete/' + this.layoutId, {}).subscribe(s => {
      this.loadLayouts();
    });
  }

  selectLayout() {
    this.layout = this.layouts.find(l => l.id === this.layoutId);
  }

  create() {
    this.http.post(environment.hostUrl + '/api/admin/layout', {}).subscribe(e => {
      this.loadLayouts();
    });
  }

  loadLayouts() {
    this.http.get(environment.hostUrl + '/api/admin/layout').subscribe(e => {
      this.layouts = <any[]>e;
      this.selectLayout();
    });
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.layoutId = params['id'];
    });

    this.loadLayouts();
  }
}
