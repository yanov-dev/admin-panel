import {Component, ElementRef, HostBinding, HostListener, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-dynamic',
  templateUrl: './dynamic-component.html'
})
export class DynamicComponent implements OnInit {
  @Input()
  data: any;

  isVisible: boolean;

  @HostBinding('style') public style;
  @HostBinding('class') public class;

  constructor(private element: ElementRef) {

  }

  @HostListener('window:resize')
  ngDoCheck() {
    this.isVisible = this.element.nativeElement.offsetParent !== null;
  }

  setup(data: any) {
    if (!this.data)
      return;

    this.data.data = data.data;
    if (!this.data.data)
      return;

    if (this.data.data.style) {
      this.style = this.data.data.style;
    }
    if (this.data.data.class) {
      this.class = this.data.data.class;
    }
  }

  ngOnInit(): void {
    this.setup(this.data);
  }
}
