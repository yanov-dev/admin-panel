import {Component} from '@angular/core';
import {DynamicBase} from '../DynamicBase';

@Component({
  selector: 'app-dynamic-div',
  templateUrl: './dynamic-div.component.html'
})
export class DynamicDivComponent extends DynamicBase {
  constructor() {
    super();
  }
}
