import {Component, Input, OnInit} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {AppSettingsService} from '../../../services/app-settings-service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-dynamic-layout',
  templateUrl: './dynamic-layout.component.html'
})
export class DynamicLayoutComponent {

  _layoutId: string;
  widget: any;

  get layoutId(): string {
    return this._layoutId;
  }

  @Input()
  set layoutId(id: string) {
    this._layoutId = id;
    this.setup();
  }

  constructor(public appSettingsService: AppSettingsService,
              private http: HttpClient,
              public snackBar: MatSnackBar) {
  }

  saveUI() {
    this.getLayout().subscribe(e => {
      e.data = JSON.stringify(this.widget.data);
      this.http.post(environment.hostUrl + '/api/admin/layout/', e).subscribe(s => {
        this.setup();
      });
    });
  }

  getLayout(): Observable<any> {
    return this.http.get(environment.hostUrl + '/api/admin/layout/' + this.layoutId);
  }

  setup() {
    this.getLayout().subscribe(e => {
      this.widget = {
        'type': 'div',
        'data': JSON.parse((<any>e).data)
      };
    });
  }
}
