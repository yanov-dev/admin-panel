import {EventEmitter, Input, OnInit, Output} from '@angular/core';

export class DynamicBase implements OnInit {
  ngOnInit(): void {
    if (!this.data.components)
      this.data.components = [];
    this.components = this.data.components;
  }

  @Input()
  data: any;

  components: any[];

  @Output() onChanged = new EventEmitter<any>();

  change() {
    this.onChanged.emit(this);
  }

  getType(): string {
    return this.constructor.name;
  }
}
