import {Component, Input, OnInit} from '@angular/core';
import {MatSnackBar, MatTableDataSource, PageEvent} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {RawSqlResponse} from '../../../Models/RawSqlResponse';
import {DynamicBase} from '../DynamicBase';

@Component({
  selector: 'app-dynamic-table',
  templateUrl: './dynamic-table.component.html'
})
export class DynamicTableComponent extends DynamicBase implements OnInit {

  dataSource: MatTableDataSource<any>;
  displayedColumns: any;

  count: number;
  isLoading: boolean;

  isEmpty: boolean;
  error: string;

  constructor(private http: HttpClient, public snackBar: MatSnackBar) {
    super();
  }

  loadData(offset: number, count: number) {
    if (!this.data.query)
      return;

    var useCache = true;
    if (typeof(this.data.cache) === 'boolean')
      useCache = this.data.cache;

    this.isLoading = true;
    this.http.post(environment.hostUrl + '/api/sql/execute', {
      cache: useCache,
      queryId: this.data.query,
      offset: offset,
      count: count
    }).subscribe(e => {
        const response = <RawSqlResponse>e;
        this.dataSource.data = response.data.items;
        this.error = response.error;
        this.isEmpty = response.data.items.length === 0;
        this.count = response.queryCount;
        this.displayedColumns = response.data.columns;
        this.isLoading = false;
      },
      error => {
        this.snackBar.open(error.message, 'Ok', {
          duration: 2000,
        });
      });
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<any>();
    this.loadData(0, 10);
  }

  pageChanged(event: PageEvent) {
    this.loadData(event.pageSize * event.pageIndex, event.pageSize);
  }
}
