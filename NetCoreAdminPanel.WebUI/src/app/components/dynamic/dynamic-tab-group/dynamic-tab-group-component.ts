import {Component, OnInit} from '@angular/core';
import {DynamicBase} from '../DynamicBase';

@Component({
  selector: 'app-dynamic-tab-group',
  templateUrl: './dynamic-tab-group-component.html'
})
export class DynamicTabGroupComponent extends DynamicBase {
  constructor() {
    super();
  }

  addChild() {
    this.data.components.push({
      data: {
        title: 'new tab',
        components: []
      }
    });
  }
}
