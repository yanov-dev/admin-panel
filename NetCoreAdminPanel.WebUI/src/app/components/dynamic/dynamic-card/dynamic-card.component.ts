import {Component, OnInit} from '@angular/core';
import {DynamicBase} from '../DynamicBase';

@Component({
  selector: 'app-dynamic-card',
  templateUrl: './dynamic-card.component.html'
})
export class DynamicCardComponent extends DynamicBase {
  constructor() {
    super();
  }
}
