import {Component, Input, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {EditDynamicDialogComponent} from '../edit-dynamic-dialog/edit-dynamic-dialog.component';
import {AddDynamicDialogComponent} from '../add-dynamic-dialog/add-dynamic-dialog.component';
import {AppSettingsService} from '../../../../services/app-settings-service';

@Component({
  selector: 'app-edit-dynamic-button',
  templateUrl: './edit-dynamic-button.component.html'
})
export class EditDynamicButtonComponent implements OnInit {

  @Input()
  control: any;

  @Input()
  top: string = '8px';

  @Input()
  right: string = '8px';

  @Input()
  title: string;

  @Input()
  handleAdd: Function;

  devMode: boolean;

  @Input()
  names: string[] = ['div', 'card', 'chart', 'tab-group', 'table'];

  constructor(public dialog: MatDialog, private appSettingsService: AppSettingsService) {
    this.devMode = this.appSettingsService.devMode;
    this.appSettingsService.changed.subscribe(e => this.devMode = e);
  }

  addChild() {
    if (this.handleAdd) {
      this.handleAdd();
      return;
    }

    const dialogRef = this.dialog.open(AddDynamicDialogComponent, {
      data: {
        names: this.names
      },
      width: '250px',
      height: '200px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.control.data.components)
          this.control.data.components = [];
        this.control.data.components.push(
          {
            type: result,
            data: this.getDefaultData(result)
          });
      }
    });
  }

  getDefaultData(controlName: string) {
    switch (controlName) {      
      case 'table': {
        return {
          class: 'col-xl-6 col-sm-12',
          query: '',
          title: ''
        };
      }
      case 'chart' : {
        return {
          class: 'col-xl-6 col-sm-12',
          colorTheme: 'vivid',
          type: 'bar',
          title: '',
          query: ''
        };
      }
      default: {
        return {
          title: '',
          components: [],
          class: '',
          style: '',
          innerClass: '',
          innerStyle: {}
        };
      }
    }
  }

  edit() {
    console.log(this.control);
    console.log(this.control.data);
    const dialogRef = this.dialog.open(EditDynamicDialogComponent, {
      data: this.control.data,
      width: '1800px',
      height: '800px'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.control.data = result;
        this.control.change();
      }
    });
  }

  ngOnInit() {
  }

}
