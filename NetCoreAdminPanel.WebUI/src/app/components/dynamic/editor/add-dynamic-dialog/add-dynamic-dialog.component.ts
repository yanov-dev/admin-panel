import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {EditDynamicDialogComponent} from '../edit-dynamic-dialog/edit-dynamic-dialog.component';
import {DynamicBase} from '../../DynamicBase';

@Component({
  selector: 'app-add-dynamic-dialog',
  templateUrl: './add-dynamic-dialog.component.html'
})
export class AddDynamicDialogComponent {

  controls: string[];
  selected: string;
  dynamic: DynamicBase;

  constructor(public dialogRef: MatDialogRef<EditDynamicDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public inData: any) {
    this.controls = inData.names;
    this.selected = this.controls[0];
  }

  add() {
    this.dialogRef.close(this.selected);
  }
}
