import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-edit-dynamic-dialog',
  templateUrl: './edit-dynamic-dialog.component.html'
})
export class EditDynamicDialogComponent implements OnInit {
  data: any;
  lastValue: any;

  constructor(public dialogRef: MatDialogRef<EditDynamicDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public inData: any) {
    this.data = inData;
    this.lastValue = this.data;
  }

  onChange(value: any) {
    this.lastValue = value;
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.lastValue);
  }

  ngOnInit() {
  }
}
