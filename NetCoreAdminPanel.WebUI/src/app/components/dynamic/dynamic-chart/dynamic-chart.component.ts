import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {RawSqlResponse} from '../../../Models/RawSqlResponse';
import {DynamicBase} from '../DynamicBase';

@Component({
  selector: 'app-api-chart',
  templateUrl: './dynamic-chart.component.html'
})
export class ApiChartComponent extends DynamicBase implements OnInit {
  items: any;

  colorTheme: string;

  isEmpty: boolean;

  constructor(private http: HttpClient) {
    super();
  }

  ngOnInit(): void {
    if (!this.data.query)
      return;

    this.colorTheme = this.data.colorTheme || 'vivid';

    this.http.post(environment.hostUrl + '/api/sql/execute', {
      queryId: this.data.query,
    }).subscribe(e => {
        const response = <RawSqlResponse>e;
        if (this.data.type === 'line') {
          this.items = [];
          this.items.push({
            name: 'test',
            series: response.data.items
          });
        } else {
          this.items = response.data.items;
        }

        this.isEmpty = response.data.items.length === 0;
      },
    );
  }
}
