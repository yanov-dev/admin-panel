import {Component, OnInit} from '@angular/core';
import {SettingsService} from '../../services/settings.service';
import {Setting} from '../../Models/setting';

@Component({
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {

  settings: Setting[];

  constructor(private settingsService: SettingsService) {

  }

  ngOnInit(): void {
    this.loadJobs();
  }

  loadJobs() {
    this.settingsService.getAll().subscribe(e => {
      this.settings = e;
    });
  }

  update(jobStat: Setting) {
    this.settingsService.update(jobStat).subscribe(e => {
      this.loadJobs();
    });
  }
}
