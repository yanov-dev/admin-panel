import {Component, Inject, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-job-editor-dialog',
  templateUrl: './job-editor-dialog.component.html',
  styleUrls: ['./job-editor-dialog.component.css']
})
export class JobEditorDialogComponent implements OnInit {

  job: any;

  constructor(public dialogRef: MatDialogRef<JobEditorDialogComponent>,
              private http: HttpClient,
              @Inject(MAT_DIALOG_DATA) private jobData: any) {
    this.job = jobData;
  }

  ngOnInit() {
  }

  save() {
    this.dialogRef.close(this.job);
  }
}
