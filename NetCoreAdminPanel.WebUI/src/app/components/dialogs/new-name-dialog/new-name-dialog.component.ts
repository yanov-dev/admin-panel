import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-new-name-dialog',
  templateUrl: './new-name-dialog.component.html',
  styleUrls: ['./new-name-dialog.component.css']
})
export class NewNameDialogComponent {

  data: string;

  constructor(public dialogRef: MatDialogRef<NewNameDialogComponent>,
              private http: HttpClient,
              @Inject(MAT_DIALOG_DATA) private inData: any) {
    this.data = inData;
  }

  save() {
    this.dialogRef.close(this.data);
  }
}
