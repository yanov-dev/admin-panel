import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {QueryInfo} from '../../../Models/QueryInfo';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {MoveTreeNodeDialogComponent} from '../move-tree-node-dialog/move-tree-node-dialog.component';
import {SqlExecuteDialogComponent} from '../sql-execute-dialog/sql-execute-dialog.component';

@Component({
  selector: 'app-edit-query-dialog',
  templateUrl: './edit-query-dialog.component.html'
})
export class EditQueryDialogComponent implements OnInit {

  query: any;
  contexts: string[];

  isSaving: boolean;

  constructor(public dialog: MatDialog,
              public dialogRef: MatDialogRef<EditQueryDialogComponent>,
              private http: HttpClient,
              @Inject(MAT_DIALOG_DATA) private inQuery: any) {
    this.query = inQuery.query;
    console.log(this.query);
  }

  ngOnInit() {
    this.http.get(environment.hostUrl + '/api/sql/context').subscribe(e => {
        this.contexts = <string[]>e;
      }
    );
  }

  remove() {
    this.http.post(environment.hostUrl + '/api/sql/query/delete', this.query).subscribe(e => {
        this.close();
      }
    );
  }

  execute() {
    const dialogRef = this.dialog.open(SqlExecuteDialogComponent, {
      data: this.query
    });
    dialogRef.afterClosed().subscribe(result => {

    });
  }

  save() {
    this.isSaving = true;
    this.http.post(environment.hostUrl + '/api/sql/query/update', this.query).subscribe(e => {
        this.isSaving = false;
      }
    );
  }

  close() {
    this.dialogRef.close();
  }
}
