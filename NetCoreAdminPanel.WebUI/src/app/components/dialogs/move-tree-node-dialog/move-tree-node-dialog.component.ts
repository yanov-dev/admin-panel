import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FlatTreeControl} from '@angular/cdk/tree';
import {QueryInfo} from '../../../Models/QueryInfo';
import {TreeFolder} from '../../sql-queries/sql-queries.component';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {Observable, of as observableOf} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Component({
  selector: 'app-move-tree-node-dialog',
  templateUrl: './move-tree-node-dialog.component.html',
  styleUrls: ['./move-tree-node-dialog.component.css']
})
export class MoveTreeNodeDialogComponent implements OnInit {

  treeControl: FlatTreeControl<QueryInfo>;
  treeFlattener: MatTreeFlattener<TreeFolder, QueryInfo>;
  dataSource: MatTreeFlatDataSource<TreeFolder, QueryInfo>;

  constructor(public dialogRef: MatDialogRef<MoveTreeNodeDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private http: HttpClient) {
    this.treeFlattener = new MatTreeFlattener(
      this.transformer,
      this._getLevel,
      this._isExpandable,
      this._getChildren);
    this.treeControl = new FlatTreeControl<QueryInfo>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  selectNode(node: any) {
    this.dialogRef.close(node);
  }

  ngOnInit() {
    this.http.post(environment.hostUrl + '/api/sql/tree', {}).subscribe(e => {
        this.dataSource.data = <TreeFolder[]>e;
      }
    );
  }

  transformer = (node: TreeFolder, level: number) => {
    const query = new QueryInfo();
    query.name = node.name;
    query.id = node.id;
    query.level = level;
    query.expandable = !!node.children;
    query.data = node;
    return query;
  };

  private _getLevel = (node: QueryInfo) => {
    return node.level;
  };

  private _isExpandable = (node: QueryInfo) => {
    return node.expandable;
  };

  private _getChildren = (node: TreeFolder): Observable<TreeFolder[]> => {
    return observableOf(node.children);
  };

  hasChild = (_: number, _nodeData: QueryInfo) => {
    return _nodeData.expandable;
  };
}
