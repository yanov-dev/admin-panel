import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveTreeNodeDialogComponent } from './move-tree-node-dialog.component';

describe('MoveTreeNodeDialogComponent', () => {
  let component: MoveTreeNodeDialogComponent;
  let fixture: ComponentFixture<MoveTreeNodeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoveTreeNodeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveTreeNodeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
