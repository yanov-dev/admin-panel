import {Component, Inject, OnInit} from '@angular/core';
import {EditQueryDialogComponent} from '../edit-query-dialog/edit-query-dialog.component';
import {HttpClient} from '@angular/common/http';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-sql-execute-dialog',
  templateUrl: './sql-execute-dialog.component.html',
  styleUrls: ['./sql-execute-dialog.component.css']
})
export class SqlExecuteDialogComponent implements OnInit {

  query: any;
  widget: any;

  constructor(
    public dialogRef: MatDialogRef<EditQueryDialogComponent>,
    private http: HttpClient,
    @Inject(MAT_DIALOG_DATA) private inData: any
  ) {
    this.query = inData;
    this.widget = {
      query: inData.id,
      cache: false
    };
  }

  ngOnInit() {
  }
}
