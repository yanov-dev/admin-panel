import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SqlExecuteDialogComponent } from './sql-execute-dialog.component';

describe('SqlExecuteDialogComponent', () => {
  let component: SqlExecuteDialogComponent;
  let fixture: ComponentFixture<SqlExecuteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SqlExecuteDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SqlExecuteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
