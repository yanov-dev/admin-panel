﻿namespace AdminPanel.Models
{
    public class JobState
    {
        public string Name { get; set; }

        public bool IsRunning { get; set; }
        
        public bool AutorunEnabled { get; set; }
        
        public int AutorunMinutes { get; set; }
    }
}