﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AdminPanel.Models
{
    [Table("query_group")]
    internal class QueryGroupDb
    {
        [Column("parent_group_id")]
        public string ParentGroupId { get; set; }

        [Column("id")]
        public string Id { get; set; }

        [Column("name")]
        public string Name { get; set; }
    }
}