﻿namespace AdminPanel.Models
{
    public class Setting
    {
        public string Name { get; set; }

        public string Data { get; set; }
    }
}