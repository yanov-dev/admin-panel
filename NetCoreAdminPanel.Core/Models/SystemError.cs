﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminPanel.Models
{
    [Table("errors")]
    public class SystemError
    {
        public string Id { get; set; }
        
        public string ClassName { get; set; }
        
        public string Method { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }
    }
}