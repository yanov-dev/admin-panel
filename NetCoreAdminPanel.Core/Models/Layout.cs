﻿namespace AdminPanel.Models
{
    public class Layout
    {
        public string Id { get; set; }

        public bool IsPublic { get; set; }

        public string Name { get; set; }

        public string Data { get; set; }
    }
}