﻿namespace AdminPanel.Models
{
    public class JobAutorunSetting
    {
        public string Name { get; set; }
        
        public int Minutes { get; set; }
        
        public bool AutorunActive { get; set; }
    }
}