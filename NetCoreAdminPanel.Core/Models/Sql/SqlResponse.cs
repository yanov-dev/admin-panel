﻿namespace AdminPanel.Models.Sql
{
    public class SqlResponse
    {
        public SqlResponse()
        {
            Data = new SqlData();
        }

        public SqlData Data { get; set; }

        public int QueryCount { get; set; }
        
        public string Error { get; set; }
    }
}