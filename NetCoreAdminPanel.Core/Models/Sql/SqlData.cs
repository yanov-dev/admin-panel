﻿using System.Collections.Generic;

namespace AdminPanel.Models.Sql
{
    public class SqlData
    {
        public SqlData()
        {
            Columns = new List<string>();
            Items = new List<Dictionary<string, string>>();
        }

        public List<string> Columns { get; set; }

        public List<Dictionary<string, string>> Items { get; set; }
    }
}