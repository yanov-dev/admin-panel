﻿using System.Collections.Generic;

namespace AdminPanel.Models.Sql
{
    public class SqlRequest
    {
        public SqlRequest()
        {
            Parameters = new Dictionary<string, string>();
        }

        public string QueryId { get; set; }

        public Dictionary<string, string> Parameters { get; set; }

        public int Offset { get; set; }

        public int Count { get; set; }

        public bool Cache { get; set; } = true;
    }
}