﻿using System.Collections.Generic;

namespace AdminPanel.Models
{
    public class QueryGroup
    {
        public QueryGroup()
        {
            Children = new List<object>();
        }

        public string ParentGroupId { get; set; }

        public string Id { get; set; }

        public string Name { get; set; }

        public string FullName { get; set; }

        public List<object> Children { get; set; }
    }
}