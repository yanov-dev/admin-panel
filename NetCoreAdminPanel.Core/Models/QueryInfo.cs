﻿using System.Collections.Generic;

namespace AdminPanel.Models
{
    public class QueryInfo
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Sql { get; set; }

        public string Context { get; set; }

        public bool IsSqlServer { get; set; }

        public bool AllowAnonymous { get; set; }

        public bool CountAble { get; set; }

        public string GroupId { get; set; }

        public List<string> Parameters
        {
            get
            {
                var result = new List<string>();
                if (string.IsNullOrEmpty(Sql))
                    return result;

                var offset = 0;
                while (true)
                {
                    var index = Sql.IndexOf('@', offset);
                    if (index <= 0)
                        break;

                    offset = index + 1;
                    var space = Sql.IndexOf(' ', index);
                    if (space <= 0)
                        space = Sql.Length;
                    var param = Sql.Substring(index + 1, space - index - 1);
                    result.Add(param);
                }

                return result;
            }
        }
    }
}