﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AdminPanel.Models
{
    [Table("queries")]
    internal class QueryInfoDb
    {
        [Column("id")]
        public string Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("sql")]
        public string Sql { get; set; }

        [Column("context")]
        public string Context { get; set; }

        [Column("countable")]
        public bool CountAble { get; set; }

        [Column("is_sql_server")]
        public bool IsSqlServer { get; set; }

        [Column("allow_anonymous")]
        public bool AllowAnonymous { get; set; }

        [Column("group_id")]
        public string GroupId { get; set; }
    }
}