﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AdminPanel.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "errors",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ClassName = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    Method = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_errors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "JobAutorunSettings",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    AutorunActive = table.Column<bool>(nullable: false),
                    Minutes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobAutorunSettings", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "Layouts",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Data = table.Column<string>(nullable: true),
                    IsPublic = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Layouts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "queries",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    allow_anonymous = table.Column<bool>(nullable: false),
                    context = table.Column<string>(nullable: true),
                    countable = table.Column<bool>(nullable: false),
                    group_id = table.Column<string>(nullable: true),
                    is_sql_server = table.Column<bool>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    sql = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_queries", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "query_group",
                columns: table => new
                {
                    id = table.Column<string>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    parent_group_id = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_query_group", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Name = table.Column<string>(nullable: false),
                    Data = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Name);
                });

            migrationBuilder.CreateIndex(
                name: "IX_errors_Message",
                table: "errors",
                column: "Message");

            migrationBuilder.CreateIndex(
                name: "IX_queries_group_id",
                table: "queries",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "IX_query_group_parent_group_id",
                table: "query_group",
                column: "parent_group_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "errors");

            migrationBuilder.DropTable(
                name: "JobAutorunSettings");

            migrationBuilder.DropTable(
                name: "Layouts");

            migrationBuilder.DropTable(
                name: "queries");

            migrationBuilder.DropTable(
                name: "query_group");

            migrationBuilder.DropTable(
                name: "Settings");
        }
    }
}
