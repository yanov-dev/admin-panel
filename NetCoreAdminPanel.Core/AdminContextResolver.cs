﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdminPanel.Services;
using Microsoft.EntityFrameworkCore;

namespace AdminPanel
{
    internal class AdminContextResolver : IContextResolver
    {
        private readonly IContextResolver _baseResolver;
        private readonly SystemDbContext _systemDbContext;

        public AdminContextResolver(IContextResolver baseResolver, SystemDbContext systemDbContext)
        {
            _baseResolver = baseResolver;
            _systemDbContext = systemDbContext;
        }

        public DbContext GetContext(string name)
        {
            if (name == "system")
                return _systemDbContext;

            return _baseResolver.GetContext(name);
        }

        public IEnumerable<string> GetAllNames()
        {
            return new List<string> {"system"}.Concat(_baseResolver.GetAllNames());
        }
    }
}