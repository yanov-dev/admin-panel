﻿using System;
using System.Linq;
using System.Text;
using AdminPanel.Models.Sql;

namespace AdminPanel
{
    internal static class SqlExtensions
    {
        public static string InsertString(this SqlData data, string table)
        {
            if (string.IsNullOrWhiteSpace(table))
                throw new ArgumentNullException(nameof(table));

            var sb = new StringBuilder();
            sb.Append($"INSERT INTO {table} (");
            sb.Append(string.Join(", ", data.Columns));
            sb.AppendLine(")");
            sb.AppendLine("VALUES");
            sb.Append(string.Join(", \r\n",
                data.Items.Select(e =>
                    $"({string.Join(", ", data.Columns.Select(c => $"'{e[c].Replace("'", "''")}'"))})")));
            sb.Append(";");

            return sb.ToString();
        }
    }
}