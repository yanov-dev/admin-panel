﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPanel.Models.Sql;
using AdminPanel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Produces("application/json")]
    [Route("api/system")]
    public class SystemController : Controller
    {
        /// <summary>
        ///     Restart application
        /// </summary>
        [HttpPost("shutdown")]
        public void Shutdown([FromServices] IApplicationLifetime applicationLifetime)
        {
            applicationLifetime.StopApplication();
        }

        [HttpPost("contexts")]
        public IEnumerable<string> GetAllContexts(IContextResolver resolver)
        {
            return resolver.GetAllNames().Append("system");
        }
    }
}