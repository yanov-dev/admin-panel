﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;
using AdminPanel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    public class MoveTreeAction
    {
        public string Id { get; set; }

        public string ParentId { get; set; }
    }

    [Route("api/sql/tree")]
    public class SqlTreeController : Controller
    {
        private readonly ISqlQueryRepository _sqlQueryRepository;

        public SqlTreeController(ISqlQueryRepository sqlQueryRepository)
        {
            _sqlQueryRepository = sqlQueryRepository;
        }

        [HttpPost("group/create")]
        public async Task<QueryGroup> CreateGroup()
        {
            return await _sqlQueryRepository.CraeteGroup();
        }

        [HttpPost("group/delete")]
        public async Task DeleteGroup([FromBody] QueryGroup group)
        {
            await _sqlQueryRepository.DeleteGroup(group);
        }

        [HttpPost("group/update")]
        public async Task UpdateGroup([FromBody] QueryGroup group)
        {
            await _sqlQueryRepository.UpdateGroup(group);
        }

        [HttpPost("move")]
        public async Task Move([FromBody] MoveTreeAction action)
        {
            await _sqlQueryRepository.Move(action.Id, action.ParentId);
        }

        [HttpPost]
        public async Task<List<QueryGroup>> GetTree()
        {
            return await _sqlQueryRepository.GetQueryGroups();
        }
    }
}