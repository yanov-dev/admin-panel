﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;
using AdminPanel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/settings")]
    public class SettingsController : Controller
    {
        private readonly ISettingsRepository _settingsRepository;

        public SettingsController(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        /// <summary>
        ///     Get All Jobs
        /// </summary>
        [HttpGet]
        public async Task<List<Setting>> GetSettings()
        {
            return await _settingsRepository.GetAll();
        }

        [HttpPatch]
        public async Task Update([FromServices] IErrorRepository errorRepository, [FromBody] Setting setting)
        {
            try
            {
                await _settingsRepository.Set(setting).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                await errorRepository.Add(nameof(SettingsController), e);
                throw;
            }
        }
    }
}