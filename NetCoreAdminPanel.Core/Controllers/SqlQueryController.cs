﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;
using AdminPanel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/sql/query")]
    public class SqlQueryController : Controller
    {
        private readonly ISqlQueryRepository _sqlQueryRepository;

        public SqlQueryController(ISqlQueryRepository sqlQueryRepository)
        {
            _sqlQueryRepository = sqlQueryRepository;
        }

        [HttpPost("create")]
        public async Task<QueryInfo> ExecuteQuery()
        {
            return await _sqlQueryRepository.Create().ConfigureAwait(false);
        }

        [HttpPost]
        public async Task<List<QueryInfo>> GetQueries()
        {
            return await _sqlQueryRepository.GetAll().ConfigureAwait(false);
        }

        [HttpPost("update")]
        public async Task UpdateQuery([FromBody] QueryInfo info, [FromServices] IErrorRepository errorRepository)
        {
            try
            {
                await _sqlQueryRepository.Update(info).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                await errorRepository.Add(nameof(SqlQueryController), e);
            }
        }

        [HttpPost("delete")]
        public async Task RemoveQuery([FromBody] QueryInfo info)
        {
            await _sqlQueryRepository.Delete(info).ConfigureAwait(false);
        }
    }
}