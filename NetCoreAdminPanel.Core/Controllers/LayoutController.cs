﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;
using AdminPanel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/admin/layout")]
    public class LayoutController : Controller
    {
        private readonly ILayoutRepository _layoutRepository;

        public LayoutController(ILayoutRepository layoutRepository)
        {
            _layoutRepository = layoutRepository;
        }

        [HttpGet]
        public async Task<List<Layout>> GetAll()
        {
            return await _layoutRepository.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<Layout> Get(string id)
        {
            var l = await _layoutRepository.Get(id).ConfigureAwait(false);
            return l;
        }

        [HttpPost]
        public async Task<Layout> Create([FromBody] Layout layout)
        {
            if (string.IsNullOrEmpty(layout?.Id))
                return await _layoutRepository.Create();
            await _layoutRepository.Update(layout).ConfigureAwait(false);
            return layout;
        }

        [HttpPost("delete/{id}")]
        public async Task Delete(string id)
        {
            await _layoutRepository.Delete(id).ConfigureAwait(false);
        }
    }
}