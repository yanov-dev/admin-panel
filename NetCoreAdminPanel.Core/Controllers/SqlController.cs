﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPanel.Models.Sql;
using AdminPanel.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/sql")]
    public class SqlController : Controller
    {
        private readonly ISqlService _sqlService;

        public SqlController(ISqlService sqlService)
        {
            _sqlService = sqlService;
        }

        [HttpGet("context")]
        public List<string> GetContexts(
            [FromServices] IContextResolver contextResolver,
            [FromServices] SystemDbContext systemDbContext)
        {
            var rel = new AdminContextResolver(contextResolver, systemDbContext);

            return rel.GetAllNames().ToList();
        }

        [HttpPost("execute")]
        public async Task<SqlResponse> ExecuteQuery([FromBody] SqlRequest request)
        {
            return await _sqlService.Execute(request);
        }
    }
}