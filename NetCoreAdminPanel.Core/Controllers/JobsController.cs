﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPanel.Models;
using AdminPanel.Services;
using Microsoft.AspNetCore.Mvc;

namespace AdminPanel.Controllers
{
    [Route("api/jobs")]
    public class JobsController : Controller
    {
        [HttpPost("start")]
        public void Start(
            [FromServices] IJobService jobService,
            [FromBody] Dictionary<string, string> parameters)
        {
            if (parameters.TryGetValue("job_name", out var jobName))
                jobService.StartJob(jobName);
        }

        [HttpPost("stop")]
        public void Stop(
            [FromServices] IJobService jobService,
            [FromBody] Dictionary<string, string> parameters)
        {
            if (parameters.TryGetValue("job_name", out var jobName))
                jobService.StopJob(jobName);
        }

        [HttpPost("update")]
        public async Task Update(
            [FromBody] JobState jobState,
            [FromServices] IJobAutorunRepository jobAutorunRepository)
        {
            await jobAutorunRepository.Update(new JobAutorunSetting
            {
                Name = jobState.Name,
                Minutes = jobState.AutorunMinutes,
                AutorunActive = jobState.AutorunEnabled
            });
        }

        [HttpGet]
        public async Task<List<JobState>> GetStats(
            [FromServices] IJobService jobService,
            [FromServices] IJobAutorunRepository jobAutorunRepository)
        {
            var states = jobService.GetStates();

            var dic = (await jobAutorunRepository.GetAll()).ToDictionary(e => e.Name);

            foreach (var state in states)
            {
                if (dic.TryGetValue(state.Name, out var s))
                {
                    state.AutorunEnabled = s.AutorunActive;
                    state.AutorunMinutes = s.Minutes;
                }
            }

            return states;
        }
    }
}