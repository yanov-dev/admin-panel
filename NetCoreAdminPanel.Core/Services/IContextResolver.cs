﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace AdminPanel.Services
{
    public interface IContextResolver
    {
        DbContext GetContext(string name);

        IEnumerable<string> GetAllNames();
    }
}