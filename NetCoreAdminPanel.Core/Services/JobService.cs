﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AdminPanel.Jobs;
using AdminPanel.Models;
using Microsoft.Extensions.DependencyInjection;

namespace AdminPanel.Services
{
    internal class JobService : IJobService
    {
        private readonly IServiceScopeFactory _scopeFactory;

        private readonly ConcurrentDictionary<string, Type> _jobs;
        private readonly ConcurrentDictionary<string, BaseJob> _activeJobs;

        public JobService(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            _jobs = new ConcurrentDictionary<string, Type>();
            _activeJobs = new ConcurrentDictionary<string, BaseJob>();

            foreach (var type in GetAllTypes())
                if (!string.IsNullOrEmpty(type.FullName))
                    _jobs[type.FullName] = type;
        }

        internal static IEnumerable<Type> GetAllTypes()
        {
            var baseJobType = typeof(BaseJob);
            var types = Assembly.GetEntryAssembly().GetTypes();

            foreach (var type in types.Where(t => baseJobType.IsAssignableFrom(t)))
                if (!string.IsNullOrEmpty(type.FullName))
                    yield return type;
        }

        public void StartJob(string jobName)
        {
            var job = CreateJob(jobName);
            if (job == null)
                return;

            _activeJobs[jobName] = job;

            Task.Run(async () =>
            {
                await job.Start(_scopeFactory);
                _activeJobs.TryRemove(jobName, out _);
            });
        }

        public void StopJob(string jobName)
        {
            if (_activeJobs.TryGetValue(jobName, out var job))
                job.Stop();
        }

        public void StopAll()
        {
            foreach (var job in _activeJobs.Values)
                job.Stop();
        }

        public List<JobState> GetStates()
        {
            return _jobs.Keys.Select(name => new JobState
                {
                    Name = name,
                    IsRunning = IsRunning(name)
                })
                .ToList();
        }

        private BaseJob CreateJob(string jobName)
        {
            if (_jobs.TryGetValue(jobName, out var type))
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var obj = scope.ServiceProvider.GetService(type);
                    return obj as BaseJob;
                }
            }

            return null;
        }

        private bool IsRunning(string name)
        {
            return _activeJobs.ContainsKey(name);
        }
    }
}