﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AdminPanel.Models;
using Microsoft.EntityFrameworkCore;

namespace AdminPanel.Services
{
    internal class ErrorRepository : IErrorRepository
    {
        private readonly SystemDbContext _systemDbContext;

        public ErrorRepository(SystemDbContext systemDbContext)
        {
            _systemDbContext = systemDbContext;
        }

        public async Task<List<SystemError>> Get(int offset, int count)
        {
            return await _systemDbContext
                .Errors
                .AsNoTracking()
                .OrderByDescending(e => e.Date)
                .Skip(offset)
                .Take(count)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task Add(string className, Exception ex, [CallerMemberName] string caller = null)
        {
            var systemError = new SystemError
            {
                ClassName = className,
                Method = caller,
                Id = Guid.NewGuid().ToString(),
                Message = ex.Message,
                Date = DateTime.UtcNow
            };

            _systemDbContext.Errors.Add(systemError);
            await _systemDbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<int> Count()
        {
            return await _systemDbContext.Errors.CountAsync().ConfigureAwait(false);
        }
    }
}