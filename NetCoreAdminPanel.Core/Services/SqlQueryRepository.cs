﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdminPanel.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AdminPanel.Services
{
    internal class SqlQueryRepository : ISqlQueryRepository
    {
        private readonly SystemDbContext _systemDbContext;

        public SqlQueryRepository(SystemDbContext systemDbContext)
        {
            _systemDbContext = systemDbContext;
        }

        public async Task<List<QueryInfo>> GetAll()
        {
            var queries = await _systemDbContext
                .QueryInfos
                .AsNoTracking()
                .ToListAsync();

            return Mapper.Map<List<QueryInfo>>(queries);
        }

        public async Task<QueryInfo> Create()
        {
            var id = Guid.NewGuid().ToString();
            var name = "New query";

            _systemDbContext.QueryInfos.Add(new QueryInfoDb
            {
                Id = id,
                Name = name,
                CountAble = true
            });

            await _systemDbContext.SaveChangesAsync();

            return new QueryInfo
            {
                Id = id,
                Name = name,
                CountAble = true
            };
        }

        public async Task Move(string id, string parentId)
        {
            if (string.IsNullOrWhiteSpace(id))
                return;

            if (id.Equals(parentId))
                return;

            var query = await _systemDbContext
                .QueryInfos
                .FindAsync(id);

            if (query != null)
            {
                query.GroupId = parentId;
                _systemDbContext.Update(query);
            }
            else
            {
                var group = await _systemDbContext
                    .QueryGroups
                    .FindAsync(id);

                if (group != null)
                {
                    group.ParentGroupId = parentId;
                    _systemDbContext.Update(group);
                }
            }

            await _systemDbContext.SaveChangesAsync();
        }

        public async Task Delete(QueryInfo info)
        {
            _systemDbContext.QueryInfos.Remove(Mapper.Map<QueryInfoDb>(info));
            await _systemDbContext.SaveChangesAsync();
        }

        public async Task Update(QueryInfo queryInfo)
        {
            var dbQuery = Mapper.Map<QueryInfoDb>(queryInfo);

            _systemDbContext.QueryInfos.Update(dbQuery);
            await _systemDbContext.SaveChangesAsync();
        }

        public async Task DeleteGroup(QueryGroup group)
        {
            var queries = await _systemDbContext
                .QueryInfos
                .AsNoTracking()
                .Where(e => e.GroupId == group.Id)
                .ToListAsync()
                .ConfigureAwait(false);

            foreach (var q in queries)
            {
                q.GroupId = null;
                _systemDbContext.Update(q);
            }

            _systemDbContext.QueryGroups.Remove(Mapper.Map<QueryGroupDb>(group));

            await _systemDbContext.SaveChangesAsync();
        }

        public async Task UpdateGroup(QueryGroup group)
        {
            _systemDbContext.QueryGroups.Update(Mapper.Map<QueryGroupDb>(group));

            await _systemDbContext.SaveChangesAsync();
        }

        public async Task<List<QueryGroup>> GetQueryGroups()
        {
            var nullGroup = new QueryGroup
            {
                Name = "NULL"
            };

            var result = new Dictionary<string, QueryGroup>();
            result[string.Empty] = nullGroup;

            var groupsRaw = await _systemDbContext
                .QueryGroups
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);

            foreach (var groupDb in groupsRaw)
                result[groupDb.Id] = Mapper.Map<QueryGroup>(groupDb);

            foreach (var g in result.Values)
            {
                var parentId = g.ParentGroupId;

                if (string.IsNullOrEmpty(parentId))
                    continue;

                result[parentId].Children.Add(g);
            }

            foreach (var g in result.Values)
            {
                var parentId = g.ParentGroupId;
                var name = g.Name;

                while (!string.IsNullOrEmpty(parentId))
                {
                    var parent = result[parentId];

                    name = $"{parent.Name} \\ {name}";

                    parentId = parent.ParentGroupId;
                }

                g.FullName = name;
            }

            var rawQueries = await _systemDbContext
                .QueryInfos
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);

            var queries = Mapper.Map<List<QueryInfo>>(rawQueries);

            foreach (var query in queries)
            {
                var id = query.GroupId ?? string.Empty;

                result[id].Children.Add(query);
            }

            return result.Values.Where(e => string.IsNullOrEmpty(e.ParentGroupId)).ToList();
        }

        public async Task<QueryGroup> CraeteGroup()
        {
            var queryGroup = new QueryGroup();

            queryGroup.Id = Guid.NewGuid().ToString();
            queryGroup.Name = "New group";

            var groupDb = Mapper.Map<QueryGroupDb>(queryGroup);

            _systemDbContext.QueryGroups.Add(groupDb);
            await _systemDbContext.SaveChangesAsync();

            return queryGroup;
        }
    }
}