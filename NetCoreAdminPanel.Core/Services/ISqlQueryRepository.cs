﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;

namespace AdminPanel.Services
{
    public interface ISqlQueryRepository
    {
        Task<QueryGroup> CraeteGroup();

        Task DeleteGroup(QueryGroup group);

        Task UpdateGroup(QueryGroup group);

        Task<List<QueryGroup>> GetQueryGroups();

        Task<List<QueryInfo>> GetAll();

        Task<QueryInfo> Create();

        Task Move(string id, string parentId);

        Task Delete(QueryInfo info);

        Task Update(QueryInfo queryInfo);
    }
}