﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;
using Microsoft.EntityFrameworkCore;

namespace AdminPanel.Services
{
    internal class JobAutorunRepository : IJobAutorunRepository
    {
        private readonly SystemDbContext _systemDbContext;

        public JobAutorunRepository(SystemDbContext systemDbContext)
        {
            _systemDbContext = systemDbContext;
        }

        public async Task<List<JobAutorunSetting>> GetAll()
        {
            return await _systemDbContext
                .JobAutorunSettings
                .ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task Update(JobAutorunSetting setting)
        {
            if (setting.AutorunActive)
                setting.Minutes = Math.Max(5, setting.Minutes);
            
            if (await _systemDbContext.JobAutorunSettings.AnyAsync(e => e.Name == setting.Name))
                _systemDbContext.Update(setting);
            else
                _systemDbContext.Add(setting);

            await _systemDbContext.SaveChangesAsync();
        }
    }
}