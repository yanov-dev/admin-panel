﻿using System.Threading.Tasks;
using AdminPanel.Models.Sql;

namespace AdminPanel.Services
{
    public interface ISqlService
    {
        Task<SqlResponse> Execute(SqlRequest request);
    }
}