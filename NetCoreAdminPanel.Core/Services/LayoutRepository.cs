﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;
using Microsoft.EntityFrameworkCore;

namespace AdminPanel.Services
{
    internal class LayoutRepository : ILayoutRepository
    {
        private readonly SystemDbContext _systemDbContext;

        public LayoutRepository(SystemDbContext systemDbContext)
        {
            _systemDbContext = systemDbContext;
        }

        public async Task<List<Layout>> GetAll()
        {
            return await _systemDbContext
                .Layouts
                .ToListAsync();
        }

        public async Task<Layout> Get(string id)
        {
            return await _systemDbContext
                .Layouts
                .FindAsync(id)
                .ConfigureAwait(false);
        }

        public async Task Update(Layout layout)
        {
            _systemDbContext.Layouts.Update(layout);
            await _systemDbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task<Layout> Create()
        {
            var layout = new Layout
            {
                Id = Guid.NewGuid().ToString(),
                Data = "{}",
                IsPublic = false,
                Name = "New layout"
            };

            _systemDbContext.Add(layout);
            await _systemDbContext.SaveChangesAsync();

            return layout;
        }

        public async Task Delete(string id)
        {
            var layout = await Get(id);
            if (layout != null)
            {
                _systemDbContext.Layouts.Remove(layout);
                await _systemDbContext.SaveChangesAsync();
            }
        }
    }
}