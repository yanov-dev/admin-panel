﻿using System.Collections.Generic;
using AdminPanel.Models;

namespace AdminPanel.Services
{
    public interface IJobService
    {
        void StartJob(string jobName);

        void StopJob(string jobName);

        void StopAll();

        List<JobState> GetStates();
    }
}