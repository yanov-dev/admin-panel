﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace AdminPanel.Services
{
    internal class SettingsRepository : ISettingsRepository
    {
        private readonly SystemDbContext _systemDbContext;

        public SettingsRepository(SystemDbContext systemDbContext)
        {
            _systemDbContext = systemDbContext;
        }

        public async Task<T> Get<T>(string name, T defaultValue) where T : class
        {
            var setting = await _systemDbContext
                .Settings
                .AsNoTracking()
                .FirstOrDefaultAsync(e => e.Name == name);

            if (setting != null)
                return JsonConvert.DeserializeObject<T>(setting.Data);

            _systemDbContext.Settings.Add(new Setting
            {
                Name = name,
                Data = JsonConvert.SerializeObject(defaultValue)
            });
            await _systemDbContext.SaveChangesAsync();
            return defaultValue;
        }

        public async Task Set<T>(string name, T t) where T : class
        {
            await Set(new Setting
            {
                Name = name,
                Data = JsonConvert.SerializeObject(t)
            }).ConfigureAwait(false);
        }

        public async Task Set(Setting setting)
        {
            if (await _systemDbContext
                    .Settings
                    .AsNoTracking()
                    .FirstOrDefaultAsync(e => e.Name == setting.Name) == null)
                _systemDbContext.Settings.Add(setting);
            else
                _systemDbContext.Settings.Update(setting);

            await _systemDbContext.SaveChangesAsync();

            _systemDbContext.Entry(setting).State = EntityState.Detached;
        }

        public async Task<List<Setting>> GetAll()
        {
            return await _systemDbContext
                .Settings
                .AsNoTracking()
                .ToListAsync()
                .ConfigureAwait(false);
        }
    }
}