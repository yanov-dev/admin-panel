﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;

namespace AdminPanel.Services
{
    public interface ILayoutRepository
    {
        Task<List<Layout>> GetAll();

        Task<Layout> Get(string id);

        Task Update(Layout layout);

        Task<Layout> Create();

        Task Delete(string id);
    }
}