﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using AdminPanel.Models;

namespace AdminPanel.Services
{
    public interface IErrorRepository
    {
        Task<List<SystemError>> Get(int offset, int count);

        Task Add(string className, Exception ex, [CallerMemberName] string caller = null);

        Task<int> Count();
    }
}