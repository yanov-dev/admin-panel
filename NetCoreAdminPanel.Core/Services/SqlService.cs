﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using AdminPanel.Models;
using AdminPanel.Models.Sql;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace AdminPanel.Services
{
    internal class SqlService : ISqlService
    {
        private readonly IContextResolver _contextResolver;

        private readonly IMemoryCache _memoryCache;
        private readonly SystemDbContext _systemDbContext;

        public SqlService(
            IMemoryCache memoryCache,
            IContextResolver contextResolver,
            SystemDbContext systemDbContext)
        {
            _memoryCache = memoryCache;
            _contextResolver = new AdminContextResolver(contextResolver, systemDbContext);
            _systemDbContext = systemDbContext;
        }

        public async Task<SqlResponse> Execute(string context, string sqlRaw)
        {
            var result = new SqlResponse();

            using (var conn = _contextResolver.GetContext(context).Database.GetDbConnection())
            {
                await conn.OpenAsync();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sqlRaw;

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (!reader.HasRows)
                            return null;

                        for (var i = 0; i < reader.FieldCount; i++)
                            result.Data.Columns.Add(reader.GetName(i));

                        while (await reader.ReadAsync())
                        {
                            var item = new Dictionary<string, string>();
                            for (var i = 0; i < reader.FieldCount; i++)
                                item[result.Data.Columns[i]] = reader[i].ToString();

                            result.Data.Items.Add(item);
                        }

                        return result;
                    }
                }
            }
        }

        public async Task<SqlResponse> Execute(SqlRequest request)
        {
            try
            {
                if (!request.Cache)
                    return await ExecuteNonCache(request).ConfigureAwait(false);

                var reqId = $"{request.QueryId}_{request.Offset}_{request.Count}";
                if (_memoryCache.TryGetValue(reqId, out SqlResponse r))
                    return r;

                var response = await ExecuteNonCache(request).ConfigureAwait(false);

                _memoryCache.Set(reqId, response, TimeSpan.FromMinutes(10));

                return response;
            }
            catch (Exception ex)
            {
                return new SqlResponse
                {
                    Error = ex.Message
                };
            }
        }

        private async Task<SqlResponse> ExecuteNonCache(SqlRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var result = new SqlResponse();

            var queryInfo = await GetQuery(request.QueryId);
            if (queryInfo.CountAble)
            {
                var count = await GetQueryCount(queryInfo, request);
                result.QueryCount = count;

                if (count == 0)
                    return result;
            }

            using (var conn = _contextResolver.GetContext(queryInfo.Context).Database.GetDbConnection())
            {
                await conn.OpenAsync();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = queryInfo.CountAble
                        ? queryInfo.IsSqlServer
                            ? $"{queryInfo.Sql} OFFSET {request.Offset} ROWS FETCH NEXT {request.Count} ROWS ONLY"
                            : $"select * from ({queryInfo.Sql}) limit {request.Count} OFFSET {request.Offset}"
                        : queryInfo.Sql;

                    if (queryInfo.CountAble && queryInfo.IsSqlServer)
                    {
                        var index = cmd.CommandText.ToLower().IndexOf("top");

                        var index2 = cmd.CommandText.IndexOf(" ", index + 1);

                        cmd.CommandText = cmd.CommandText.Remove(index, index2 - index + 1);
                    }

                    AddParameters(cmd, request);

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (!reader.HasRows)
                            return null;

                        for (var i = 0; i < reader.FieldCount; i++)
                            result.Data.Columns.Add(reader.GetName(i));

                        while (await reader.ReadAsync())
                        {
                            var item = new Dictionary<string, string>();
                            for (var i = 0; i < reader.FieldCount; i++)
                                item[result.Data.Columns[i]] = reader[i].ToString();

                            result.Data.Items.Add(item);
                        }

                        return result;
                    }
                }
            }
        }

        private async Task ExecuteRaw(string context, string sqlRaw)
        {
            if (string.IsNullOrWhiteSpace(context)) throw new ArgumentNullException(nameof(context));
            if (string.IsNullOrWhiteSpace(sqlRaw)) throw new ArgumentNullException(nameof(sqlRaw));

            using (var conn = _contextResolver.GetContext(context).Database.GetDbConnection())
            {
                await conn.OpenAsync();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sqlRaw;
                    await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                }
            }
        }

        private void AddParameters(DbCommand command, SqlRequest request)
        {
            foreach (var parameter in request.Parameters)
            {
                var param = command.CreateParameter();
                param.ParameterName = parameter.Key;
                param.Value = parameter.Value;
                command.Parameters.Add(param);
            }
        }

        private async Task<int> GetQueryCount(QueryInfoDb queryInfo, SqlRequest request)
        {
            using (var conn = _contextResolver.GetContext(queryInfo.Context).Database.GetDbConnection())
            {
                await conn.OpenAsync();

                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = $"select count(*) from ({queryInfo.Sql})";
                    if (queryInfo.IsSqlServer)
                        cmd.CommandText += " q";
                    AddParameters(cmd, request);

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        return reader.GetInt32(0);
                    }
                }
            }
        }

        private async Task<QueryInfoDb> GetQuery(string queryId)
        {
            return await _systemDbContext.QueryInfos.AsNoTracking().FirstAsync(e => e.Id == queryId);
        }
    }
}