﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;

namespace AdminPanel.Services
{
    public interface IJobAutorunRepository
    {
        Task<List<JobAutorunSetting>> GetAll();

        Task Update(JobAutorunSetting setting);
    }
}