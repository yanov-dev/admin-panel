﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdminPanel.Models;

namespace AdminPanel.Services
{
    public interface ISettingsRepository
    {
        Task<T> Get<T>(string name, T defaultValue) where T : class;

        Task Set<T>(string name, T t) where T : class;

        Task Set(Setting setting);

        Task<List<Setting>> GetAll();
    }
}