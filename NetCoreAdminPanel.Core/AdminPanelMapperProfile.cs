﻿using AdminPanel.Models;
using AutoMapper;

namespace AdminPanel
{
    public class AdminPanelMapperProfile : Profile
    {
        public AdminPanelMapperProfile()
        {
            CreateMap<QueryInfoDb, QueryInfo>();
            CreateMap<QueryInfo, QueryInfoDb>();

            CreateMap<QueryGroupDb, QueryGroup>();
            CreateMap<QueryGroup, QueryGroupDb>();
        }
    }
}