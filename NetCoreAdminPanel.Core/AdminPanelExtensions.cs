﻿using System;
using System.Reflection;
using AdminPanel.Jobs;
using AdminPanel.Models;
using AdminPanel.Services;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AdminPanel
{
    public static class AdminPanelExtensions
    {
        public static void AddAdminPanel<T>(this IServiceCollection services,
            Action<DbContextOptionsBuilder> builder)
            where T : class, IContextResolver
        {
            if (builder == null)
                throw new ArgumentNullException($"{nameof(builder)} is null");

            services.AddDbContext<SystemDbContext>(builder);

            services.AddTransient<IJobAutorunRepository, JobAutorunRepository>();
            services.AddTransient<ISqlService, SqlService>();
            services.AddTransient<ISettingsRepository, SettingsRepository>();
            services.AddTransient<IContextResolver, T>();
            services.AddTransient<ILayoutRepository, LayoutRepository>();
            services.AddTransient<IErrorRepository, ErrorRepository>();
            services.AddTransient<ISqlQueryRepository, SqlQueryRepository>();

            services.AddSingleton<IHostedService, InitJobService>();

            foreach (var type in JobService.GetAllTypes())
                services.AddTransient(type);

            services.AddSingleton<IJobService, JobService>();
            
            services.AddHangfire(config => config.UseMemoryStorage());
        }

        public static void UseAdminPanel(this IApplicationBuilder app)
        {
            app.UseHangfireDashboard();
            app.UseHangfireServer();
        }
    }
}