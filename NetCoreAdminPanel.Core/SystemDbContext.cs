﻿using System;
using AdminPanel.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace AdminPanel
{
    public class SystemDbContext : DbContext, IDesignTimeDbContextFactory<SystemDbContext>
    {
        public SystemDbContext(DbContextOptions<SystemDbContext> options)
            : base(options)
        {
        }

        public SystemDbContext()
        {
        }

        public DbSet<JobAutorunSetting> JobAutorunSettings { get; set; }
        
        public DbSet<Layout> Layouts { get; set; }

        public DbSet<Setting> Settings { get; set; }

        public DbSet<SystemError> Errors { get; set; }

    internal DbSet<QueryGroupDb> QueryGroups { get; set; }

        internal DbSet<QueryInfoDb> QueryInfos { get; set; }

        public SystemDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<SystemDbContext>();
            optionsBuilder.UseSqlite($"Data Source={Guid.NewGuid()}.db");

            return new SystemDbContext(optionsBuilder.Options);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<JobAutorunSetting>().HasKey(e => e.Name);
            
            builder.Entity<QueryInfoDb>().HasKey(e => e.Id);
            builder.Entity<QueryInfoDb>().HasIndex(e => e.GroupId);

            builder.Entity<Layout>().HasKey(e => e.Id);

            builder.Entity<QueryGroupDb>().HasKey(e => e.Id);
            builder.Entity<QueryGroupDb>().HasIndex(e => e.ParentGroupId);

            builder.Entity<Setting>()
                .HasKey(e => e.Name);

            builder.Entity<SystemError>()
                .HasKey(e => e.Id);

            builder.Entity<SystemError>()
                .HasIndex(e => e.Message);
        }
    }
}