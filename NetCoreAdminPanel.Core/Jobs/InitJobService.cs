﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AdminPanel.Services;
using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AdminPanel.Jobs
{
    public class InitJobService : IHostedService
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public InitJobService(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            try
            {
                using (var score = _scopeFactory.CreateScope())
                {
                    var db = score.ServiceProvider.GetRequiredService<SystemDbContext>().Database;
                    await db.MigrateAsync(cancellationToken);

                    var jobAutorunRepo = score.ServiceProvider.GetRequiredService<IJobAutorunRepository>();

                    var settings = (await jobAutorunRepo.GetAll()).Where(e => e.AutorunActive).ToList();

                    if (settings.Count > 0)
                    {
                        var jobService = score.ServiceProvider.GetRequiredService<IJobService>();
                        foreach (var setting in settings)
                        {
                            RecurringJob.AddOrUpdate(setting.Name,
                                () => jobService.StartJob(setting.Name),
                                Cron.MinuteInterval(setting.Minutes));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                using (var scope = _scopeFactory.CreateScope())
                {
                    var errorRepo = scope.ServiceProvider.GetRequiredService<IErrorRepository>();

                    await errorRepo.Add(nameof(InitJobService), e);
                }
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}