﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using AdminPanel.Services;
using Microsoft.Extensions.DependencyInjection;

namespace AdminPanel.Jobs
{
    public abstract class BaseJob
    {
        private CancellationTokenSource _cts;

        public async Task Start(IServiceScopeFactory scopeFactory)
        {
            Stop();
            _cts = new CancellationTokenSource();

            if (scopeFactory == null)
                throw new ArgumentNullException(nameof(scopeFactory));

            try
            {
                await DoWork(scopeFactory, _cts.Token).ConfigureAwait(false);
            }
            catch (TaskCanceledException ex)
            {
                Debug.WriteLine(ex);
            }
            catch (Exception e)
            {
                using (var scope = scopeFactory.CreateScope())
                {
                    var errorRepository = scope.ServiceProvider.GetRequiredService<IErrorRepository>();
                    await errorRepository.Add(GetType().Name, e);
                }
            }
        }

        public void Stop()
        {
            _cts?.Cancel();
        }

        protected abstract Task DoWork(IServiceScopeFactory scopeFactory, CancellationToken ct);
    }
}